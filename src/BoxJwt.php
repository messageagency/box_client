<?php

namespace Drupal\box_client;

use OAuth\OAuth2\Service\AbstractService;
use OAuth\OAuth2\Token\StdOAuth2Token;
use OAuth\Common\Http\Exception\TokenResponseException;
use OAuth\Common\Http\Uri\Uri;
use OAuth\Common\Consumer\CredentialsInterface;
use OAuth\Common\Http\Client\ClientInterface;
use OAuth\Common\Storage\TokenStorageInterface;
use OAuth\Common\Http\Uri\UriInterface;

class BoxJwt extends AbstractService
{

  /**
   * @var \Drupal\box_client\BoxDrupalCacheStorage
   */
  public $storage;

  public function __construct(
    CredentialsInterface $credentials,
    ClientInterface $httpClient,
    TokenStorageInterface $storage,
    $scopes = array(),
    UriInterface $baseApiUri = null
  ) {
    parent::__construct($credentials, $httpClient, $storage, $scopes, $baseApiUri, true);

    if (null === $baseApiUri) {
      $this->baseApiUri = new Uri('https://api.box.com/2.0/');
    }
  }

  /*
   * Required to be implement by the abstract class but
   * */

  public function getAuthorizationEndpoint()
  {
    return new Uri('https://account.box.com/api/oauth2/authorize');
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessTokenEndpoint()
  {
    return new Uri('https://api.box.com/oauth2/token');
  }

  /**
   * {@inheritdoc}
   */
  protected function getAuthorizationMethod()
  {
    return static::AUTHORIZATION_METHOD_HEADER_BEARER;
  }

  /**
   * {@inheritdoc}
   */
  protected function parseAccessTokenResponse($responseBody)
  {
    $data = json_decode($responseBody, true);

    if (null === $data || !is_array($data)) {
      throw new TokenResponseException('Unable to parse response.');
    }
    elseif (isset($data['error'])) {
      throw new TokenResponseException('Error in retrieving token: "' . $data['error'] . '"');
    }

    $token = new StdOAuth2Token();
    $token->setAccessToken($data['access_token']);
    $token->setLifeTime($data['expires_in']);

    if (isset($data['refresh_token'])) {
      $token->setRefreshToken($data['refresh_token']);
      unset($data['refresh_token']);
    }

    unset($data['access_token']);
    unset($data['expires_in']);

    $token->setExtraParams($data);

    return $token;
  }

  /**
   * {@inheritdoc}
   */
  public function requestAccessToken($code, $state = null)
  {
    /**
     * Removed AuthFactory Check, JWT does not need AuthFactory, it uses a signed request instead.
     * Lack of AuthFactory check means we also don't need the code or care about the state, this function takes the arguments
     * in order to fulfill the contract of the interface but does not use them. This is pretty hacky because
     * the library was not really built to utilize JWT and Box implements the JWT concept a little bit different than
     * seems to be intended by spec on top of it.
     *
     * Box Doc: https://developer.box.com/docs/authentication-with-jwt
     * JWT Spec: https://jwt.io/
     * */

    $bodyParams = array(
      'grant_type'    => 'urn:ietf:params:oauth:grant-type:jwt-bearer',
      'client_id'     => $this->credentials->getConsumerId(),
      'client_secret' => $this->credentials->getConsumerSecret(),
      'assertion'     => $this->credentials->getAssertion(),
    );

    $responseBody = $this->httpClient->retrieveResponse(
      $this->getAccessTokenEndpoint(),
      $bodyParams,
      $this->getExtraOAuthHeaders()
    );

    $token = $this->parseAccessTokenResponse($responseBody);
    $this->storage->storeAccessToken($this->service(), $token);

    return $token;
  }

}
