<?php

namespace Drupal\box_client;

use Exception;

class BoxHttpClientResponse {
  public $body;
  public $headers;
  public $code;
  public $body_file_path;
  public $body_file_handle;

  function __construct() {
    $this->headers = new BoxHttpClientHeaders();
  }

  function deleteBodyFile() {
    if ($this->body_file_path != NULL) {
      $result = fclose($this->body_file_handle);
      if ($result !== TRUE) {
        throw new Exception("Error closing body_file_handle for '{$this->body_file_path}: " . print_r(error_get_last(), TRUE));
      }
      $this->body_file_handle = NULL;
      $result = unlink($this->body_file_path);
      if ($result !== TRUE) {
        throw new Exception("Error unlinking body_file_path '{$this->body_file_path}: " . print_r(error_get_last(), TRUE));

      }
      $this->body_file_path = NULL;
    }
  }

  function rewind() {
    $result = fseek($this->body_file_handle, 0);
    if ($result !== 0) {
      throw new Exception("Error seeking to beginning of body_file_handle for '{$this->body_file_path}': " . print_r(error_get_last(), TRUE));
    }
  }

  function setBodyFile($path, $handle) {
    $this->body_file_path = $path;
    $this->body_file_handle = $handle;
    $this->rewind();
  }
}

