<?php

namespace Drupal\box_client;

use OAuth\Common\Http\Client\AbstractClient;
use OAuth\Common\Http\Uri\UriInterface;
use OAuth\Common\Http\Exception\TokenResponseException;

class BoxHttpClient extends AbstractClient
{
  public $body_file_path;
  public $stashed_body_file;
  public $parameters = array();
  public $response;

  public function getTimeout() {
    return $this->timeout;
  }

  public function retrieveResponse(UriInterface $endpoint, $requestBody, array $extraHeaders = array(), $method = 'POST') {
    $this->response = new BoxHttpClientResponse();
    $method = strtoupper($method);

    $this->normalizeHeaders($extraHeaders);

    if ($method === 'GET' && !empty($requestBody)) {
      throw new \InvalidArgumentException('No body expected for "GET" request.');
    }

    $extraHeaders['Host']       = 'Host: '.$endpoint->getHost();
    $extraHeaders['Connection'] = 'Connection: close';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $endpoint->getAbsoluteUri());
    $result = curl_setopt($ch, CURLOPT_HEADERFUNCTION, array($this->response->headers, 'store_header_data'));

    if ($method === 'POST' || $method === 'PUT') {
      /*
       * This hacky check against the URL is because the token request
       * needs to be url encoded, but file uploads need to be multi-part
       * encoded. Since I think the multi-part one is the more general case
       * I added this check. I didn't want to modify the oAuth library.
       */
      if ($endpoint->getAbsoluteUri() == 'https://www.box.com/api/oauth2/token' && $requestBody && is_array($requestBody)) {
        $requestBody = http_build_query($requestBody, '', '&');
        $extraHeaders['Content-type'] = 'Content-type: application/x-www-form-urlencoded';
      } 
      if ($method === 'PUT') {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
      } else {
        curl_setopt($ch, CURLOPT_POST, true);
      }
      curl_setopt($ch, CURLOPT_POSTFIELDS, $requestBody);
    } else {
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    }

    if ($this->maxRedirects > 0) {
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_MAXREDIRS, $this->maxRedirects);
    }

    curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $extraHeaders);
    curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);

    foreach ($this->parameters as $key => $value) {
      curl_setopt($ch, $key, $value);
    }

    $result = curl_exec($ch);
    $this->response->code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($this->usingBodyFile()) {
      $this->response->setBodyFile($this->body_file_path, $this->parameters[CURLOPT_FILE]);
      $this->body_file_path = NULL;
    } else {
      $this->response->body = $result;
    }

    if (false === $this->response->body) {
      $errNo  = curl_errno($ch);
      $errStr = curl_error($ch);
      curl_close($ch);
      if (empty($errStr)) {
        throw new TokenResponseException('Failed to request resource.', $this->response->code);
      }
      throw new TokenResponseException('cURL Error # '.$errNo.': '.$errStr, $this->response->code);
    }
    curl_close($ch);

    $this->response->headers->parse();

    return $this->response->body;
  }

  public function setDownloadToTempBodyFile() {
    if ($this->body_file_path != NULL) {
      throw new \Exception("You cannot call setDownloadToTempBodyFile if it has already been called.");
    }
    $this->body_file_path = \Drupal::service('file_system')->tempnam('temporary://', 'file');
    $temp_file = fopen($this->body_file_path, 'w+');
    if ($temp_file === FALSE) {
      throw new \Exception("Error opening temp body file '{$this->body_file_path}': " . print_r(error_get_last(), TRUE));
    }
    $this->parameters[CURLOPT_FILE] = $temp_file;
  }

  public function stashBodyFile() {
    if ($this->usingBodyFile()) {
      $this->stashed_body_file = $this->parameters[CURLOPT_FILE];
      unset($this->parameters[CURLOPT_FILE]);
    }
  }

  public function unstashBodyFile() {
    if ($this->stashed_body_file != NULL) {
      $this->parameters[CURLOPT_FILE] = $this->stashed_body_file;
      $this->stashed_body_file = NULL;
    }
  }

  public function usingBodyFile() {
    return (array_key_exists(CURLOPT_FILE, $this->parameters) && $this->parameters[CURLOPT_FILE] != NULL);
  }
}
