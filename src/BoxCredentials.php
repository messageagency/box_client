<?php

namespace Drupal\box_client;

use OAuth\Common\Consumer\Credentials;
use Firebase\JWT\JWT;

class BoxCredentials extends Credentials {

  private $assertion;

  public function __construct($consumerId, $consumerSecret, $callBackUrl, $header, $claims, $key)
  {
    parent::__construct($consumerId, $consumerSecret, $callBackUrl);

    if(substr($key['file'], 0, 10) === 'private://') {
      // openssl_get_privatekey aparently doesn't correctly interpret private://
      // file stream, so go ahead and fetch it with get_contents.
      $key = openssl_get_privatekey(file_get_contents($key['file']), $key['pass']);
    }
    else {
      $json = file_get_contents($key['file']);
      $secrets = json_decode($json);
      $key = openssl_get_privatekey($secrets->box_key, $key['pass']);
    }

    $this->assertion = JWT::encode($claims, $key, $header['alg'], $header['kid'], $header);
  }

  /**
   * @return string
   */
  public function getAssertion()
  {
    return $this->assertion;
  }

}
