<?php

namespace Drupal\box_client;

abstract class BoxObjectBase {

  function __construct(array $values = array()) {
    foreach ($values as $key => $value) {
      $this->{$key} = $value;
    }
  }
  
}