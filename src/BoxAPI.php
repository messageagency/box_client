<?php

namespace Drupal\box_client;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\State\StateInterface;
use OAuth\Common\Storage\Exception\TokenNotFoundException;
use OAuth\Common\Storage\TokenStorageInterface;
use OAuth\Common\Token\Exception\ExpiredTokenException;
use OAuth\OAuth2\Token\StdOAuth2Token;
use OAuth\ServiceFactory;

class BoxAPI  {

  /**
   * @var \Drupal\box_client\BoxHttpClient
   */
  public $http_client;

  /**
   * @var \Drupal\box_client\BoxJwt
   */
  public $service;

  /**
   * @var \Drupal\box_client\BoxDrupalCacheStorage
   */
  public $storage;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  static public $VALID_RESPONSE_CODES = array(200, 201, 204, 409);

  const UPLOAD_BASE_URL = "https://upload.box.com/api/2.0";

  function __construct(TokenStorageInterface $storage, ServiceFactory $oauth_service_factory, BoxHttpClient $box_http_client, StateInterface $state, FileSystemInterface $fileSystem) {

    // @TODO separate key, headers, and claims out of this class
    $key = array(
      'file' => BOX_KEY_FILE,
      'pass'=> BOX_KEY_PASS,
    );

    $header = array(
      'alg' => BOX_ALG,
      'typ' => 'JWT',
      'kid' => BOX_KEY_ID,
    );

    $claims = array(
      'iss' => BOX_CLIENT_ID,
      'sub' => BOX_USER_ID,
      'box_sub_type' => 'user',
      'aud' => 'https://api.box.com/oauth2/token',
      'jti' => uniqid('', true),
      'exp' => time()+10,
    );

    $this->state = $state;
    $this->storage = $storage;
    $this->service_factory = $oauth_service_factory;
    $this->box_http_client = $box_http_client;
    $this->service_factory->setHttpClient($this->box_http_client);
    $credentials = new BoxCredentials(BOX_CLIENT_ID, BOX_CLIENT_SECRET, '/', $header, $claims, $key);
    $this->service_factory->registerService('BoxJwt', BoxJwt::class);
    $this->service = $this->service_factory->createService('BoxJwt', $credentials, $this->storage);
    $this->fileSystem = $fileSystem;
  }

  function getAuthorizationUri() {
    return $this->service->getAuthorizationUri();
  }

  function requestAccessToken($code, $state) {
    return $this->service->requestAccessToken($code, $state);
  }

  function createFolderOrGetExistingFolderId($folder_name, $parent_folder_id) {
    $result = $this->createFolder($folder_name, $parent_folder_id);
    if ($result['type'] == "folder") {
      return $result['id'];
    }
    else if ($result['type'] == "error") {
      if ($result['code'] == "item_name_in_use") {
        return $result['context_info']['conflicts'][0]['id'];
      }
      else {
        throw new \Exception("Got error when trying to create/find folder $folder_name in $parent_folder_id." . print_r($result, TRUE));
      }
    }
  }

  function createFolder($folder_name, $parent_folder_id) {
    $url = "/folders";
    $params = array(
      'name' => $folder_name,
      'parent' => array('id' => $parent_folder_id)
    );
    $body = json_encode($params);
    $response = $this->tryRequest($url, 'POST', $body);
    return $this->jsonResponse($response);
  }

  function getFileInfo($file_id) {
    $url = "/files/$file_id";
    return $this->tryRequest($url);
  }

  function downloadFile($file_id) {
    $url = "/files/$file_id/content";
    $original_timeout = $this->box_http_client->getTimeout();
    $this->box_http_client->setTimeout(6250);
    $this->box_http_client->setDownloadToTempBodyFile();
    try {
      $response = $this->tryRequest($url);
    }
    catch (\Exception $e) {
      $this->box_http_client->setTimeout($original_timeout);
      throw $e;
    }
    $this->box_http_client->setTimeout($original_timeout);
    return $response;
  }

  function getFolderItems($folder_id) {
    $url = "/folders/$folder_id/items";
    $response = $this->tryRequest($url);
    return $this->jsonResponse($response);
  }

  function getFolderWithNameInParentFolder($folder_name, $parent_folder_name) {
    $all_folders_in_dir = $this->getFolders($parent_folder_name);
    foreach($all_folders_in_dir as $key => $val) {
      if ($val['name'] == $folder_name) {
        return $val['id'];
      }
    }
    return false;
  }

  function getFolders($folder_id) {
    $data = $this->getFolderItems($folder_id);
    foreach($data['entries'] as $item){
      $array = '';
      if($item['type'] == 'folder'){
        $array = $item;
      }
      $return[] = $array;
    }
    return array_filter($return);
  }

  function putFile($file_name, $parent_id, $overwrite = TRUE) {
    $file = $this->fileSystem->realpath($file_name);
    if (function_exists('curl_file_create')) {
      $file = curl_file_create($file);
    }
    else {
      $file = "@" . $file;
    }
    $params =  array(
      'name' => $file,
      'parent_id' => $parent_id,
    );
    $url = self::UPLOAD_BASE_URL . "/files/content";
    $original_timeout = $this->box_http_client->getTimeout();
    $this->box_http_client->setTimeout(6250);
    try {
      $response = $this->tryRequest($url, 'POST', $params);
    }
    catch (\Exception $e) {
      $this->box_http_client->setTimeout($original_timeout);
      throw $e;
    }
    $this->box_http_client->setTimeout($original_timeout);
    $json_body = $this->jsonResponse($response);

    if ($response->code == 200 || $response->code == 201) {
      return $json_body;
    }

    // If the put fails for any reason other than naming conflict, we can't
    // resolve it. Go ahead and return the error response.
    // @TODO do we want to throw here instead?
    if (!$overwrite
    || $response->code != 409
    || empty($json_body['code'])
    || $json_body['code'] != 'item_name_in_use'
    || empty($json_body['context_info']['conflicts']['id'])) {
      return $json_body;
    }

    // Delete the name-conflicting file and upload the new one.
    // There is an API for updating file info, but it doesn't work. Womp womp.
    // @TODO archiving the old file is a customization / add-on.
    $this->deleteFile($json_body['context_info']['conflicts']['id']);
    return $this->putFile($file_name, $parent_id, FALSE);
  }

  function deleteFile($file_id) {
    $url = "/files/$file_id";
    $response = $this->tryRequest($url, 'DELETE');
    return true;
  }

  function updateFileInfo($file_id, $params) {
    $url = "/files/$file_id";
    $body = json_encode($params);
    $response = $this->tryRequest($url, 'PUT', $body);
    return $this->jsonResponse($response);
  }

  private function jsonResponse($response) {
    $result = json_decode($response->body, TRUE);
    if ($result === NULL) {
      throw new \Exception("Error parsing json reponse from Box.Net: " . print_r($response, TRUE));
    }
    return $result;
  }

  private function tryRequest($path, $method = 'GET', $body = NULL, $extraHeaders = array()) {
    $this->log('tryRequest: ' . $path . ' ' . $method . ' ' . print_r($body, true) . ' ' . print_r($extraHeaders, true));

    $retry = FALSE;
    try {
      $this->service->request($path, $method, $body, $extraHeaders);
    }
    catch (ExpiredTokenException $e) {
      $retry = TRUE;
    }
    catch (TokenNotFoundException $e) {
      $retry = TRUE;
    }
    if ($retry) {
      /*
       * If we were doing a request that saves to a file (instead of memory),
       * we need to turn that off when we make the request to refresh the
       * access token because the OAuth library shares the http_client
       * object and needs to read the body which will be small and we want
       * to just handle in memory.
       */
      $this->box_http_client->stashBodyFile();
      $this->service->requestAccessToken(NULL);
      $this->box_http_client->unstashBodyFile();
      $this->service->request($path, $method, $body, $extraHeaders);
    }
    $response = $this->box_http_client->response;
    if (!in_array($response->code, self::$VALID_RESPONSE_CODES, TRUE)) {
      // Log stack trace
      throw new \Exception("Got an unexpected response from Box.Net when requesting $path ($method) "
        . print_r($body, true) . ' ' . print_r($extraHeaders, true) . ' ' . print_r($response, TRUE));
    }
    $this->log_response($response);

    return $response;
  }

  /**
   * Return FALSE (if not error), or return the error code if Box response is
   * an error
   */
  public static function isErrorResponse(array $response) {
    if (empty($r['type']) || $r['type'] != 'error') {
      return FALSE;
    }
    return $r['code'];
  }

  private static function log_response($response) {
    $headers = $response->headers->headers;
    if (!empty($headers['content-type']) && $headers['content-type'] == 'application/json') {
      BoxAPI::log('response: ' . print_r($response, true));
    }
    else {
      $redacted = array();
      foreach ($response as $key => $val) {
        if ($key != 'body') {
          $redacted[$key] = $val;
        }
        else {
          $redacted['body'] = '<excluded from logging>';
        }
      }
      BoxAPI::log('response: ' . print_r($redacted, true));
    }
  }

  public static function log($str) {
    global $conf;
    // We're only logging when error reporting is enabled.
    \Drupal::logger('BOX')->info('<pre>%log</pre>', array('%log' => htmlentities($str)));
    return;
  }

}
