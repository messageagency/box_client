<?php

namespace Drupal\box_client;

class BoxHttpClientHeaders 
{
  public $raw_header = '';
  public $status_line;
  public $headers = NULL;

  function store_header_data($curl, $header_data) {
    $this->raw_header .= $header_data;
    return strlen($header_data);
  }

  static function parse_headers($headers_part) {
    $headers = array();
    $key = '';
    foreach(explode("\n", $headers_part) as $i => $h) {
      $h = explode(':', $h, 2);
      if (isset($h[1])) {
        $key = strtolower($h[0]);
        if (!isset($headers[$key])) {
          $headers[$key] = trim($h[1]);
        } elseif (is_array($headers[$key])) {
          $headers[$key] = array_merge($headers[$key], array(trim($h[1])));
        } else {
          $headers[$key] = array_merge(array($headers[$key]), array(trim($h[1])));
        }
      } else {
        if (substr($h[0], 0, 1) == "\t") {
          $headers[$key] .= "\r\n\t".trim($h[0]);
        } elseif (!$key) {
          $headers[0] = trim($h[0]);
          trim($h[0]);
        }
      }
    }
    return $headers; 
  }

  function parse()
  {
    $eol_pos = strpos($this->raw_header, "\r\n");
    if ($eol_pos === FALSE) {
      throw new \Exception("Error parsing HTTP response header.  Expected an EOL, but didn't find one in '{$this->raw_header}'.");
    }
    $this->status_line = substr($this->raw_header, 0, $eol_pos);
    $this->status = preg_split("/\s/", $this->status_line, 3);
    if ($this->status[1] == '100') {
      $first_status_eol_pos = $eol_pos;
      $eol_pos = strpos($this->raw_header, "\r\n", $first_status_eol_pos + 4);
      if ($eol_pos === FALSE) {
        throw new \Exception("Error parsing HTTP response header.  Expected an EOL, but didn't find one in '{$this->raw_header}'.");
      }
      $this->status_line = substr($this->raw_header, $first_status_eol_pos + 4, $eol_pos - $first_status_eol_pos - 4);
      $this->status = preg_split("/\s/", $this->status_line, 3);
    }
    $headers_part = substr($this->raw_header, $eol_pos + 2);
    $this->headers = self::parse_headers($headers_part);
  }
}
