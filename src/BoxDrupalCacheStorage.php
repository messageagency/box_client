<?php

namespace Drupal\box_client;

use Drupal\Core\Cache\CacheBackendInterface;
use OAuth\Common\Storage\Exception\TokenNotFoundException;
use OAuth\Common\Storage\Exception\AuthorizationStateNotFoundException;
use OAuth\Common\Storage\TokenStorageInterface;
use OAuth\Common\Token\TokenInterface;

class BoxDrupalCacheStorage implements TokenStorageInterface
{
  const BOX_OAUTH_TOKENS_CACHE_ID = "box_auth2:tokens";
  const BOX_OAUTH_STATES_CACHE_ID = "box_auth2:states";

  public $tokens = array();
  public $states = array();

  protected $cache;
  protected $state;

  public function __construct(CacheBackendInterface $cache) {
    $this->cache = $cache;
    $cache_entry = $this->cache->get(BoxDrupalCacheStorage::BOX_OAUTH_TOKENS_CACHE_ID);
    $this->tokens = ['Box' => []];
    $this->states= ['Box' => []];
    if ($cache_entry !== FALSE) {
      $this->tokens = $cache_entry->data;
    }
    $cache_entry = $this->cache->get(BoxDrupalCacheStorage::BOX_OAUTH_STATES_CACHE_ID);
    if ($cache_entry !== FALSE) {
      $this->states = $cache_entry->data;
    }
  }

  public function cacheStates() {
    $this->cache->set(BoxDrupalCacheStorage::BOX_OAUTH_STATES_CACHE_ID, $this->states);
  }

  public function cacheTokens() {
    $this->cache->set(BoxDrupalCacheStorage::BOX_OAUTH_TOKENS_CACHE_ID, $this->tokens);
  }

  public function retrieveAccessToken($service) {
    if ($this->hasAccessToken($service)) {
      return $this->tokens[$service];
    }
    throw new TokenNotFoundException('Token not stored');
  }

  public function storeAccessToken($service, TokenInterface $token) {
    $this->tokens[$service] = $token;
    $this->cacheTokens();
    return $this;
  }

  public function hasAccessToken($service) {
    return array_key_exists($service, $this->tokens);
  }

  public function clearToken($service) {
    if ($this->hasAccessToken($service)) {
      unset($this->tokens[$service]);
    }
    $this->cacheTokens();
    return $this;
  }

  public function clearAllTokens() {
    $this->tokens = array();
    $this->cacheTokens();
    return $this;
  }

  public function retrieveAuthorizationState($service) {
    if ($this->hasAuthorizationState($service)) {
      return $this->states[$service];
    }
    throw new AuthorizationStateNotFoundException('AuthorizationState not stored');
  }

  public function storeAuthorizationState($service, $state) {
    $this->states[$service] = $state;
    $this->cacheStates();
    return $this;
  }

  public function hasAuthorizationState($service) {
    return array_key_exists($service, $this->states);
  }

  public function clearAuthorizationState($service) {
    if ($this->hasAuthorizationState($service)) {
      unset($this->states[$service]);
    }
    $this->cacheStates();
    return $this;
  }

  public function clearAllAuthorizationStates() {
    $this->states = array();
    $this->cacheStates();
    return $this;
  }
}
